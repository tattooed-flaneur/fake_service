# -*- coding: utf-8 -*-

from flask import Flask, jsonify, request
import json
import os

app = Flask(__name__)


here = os.path.split(__file__)[0]


with open(os.path.join(here, 'flaneur.jsonl')) as d:
    data = [json.loads(line) for line in d]

print(data)

@app.route("/")
def hello():
    q = request.args.get('q')
    if not q:
        result = data
    else:
        q = q.lower()
        result = [item for item in data if 'name' in item and q in item['name'].lower()]
    return jsonify(result)


def run_service():
    app.run()


if __name__=='__main__':
    run_service()
